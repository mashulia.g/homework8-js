let input = document.getElementById('price')
let span = document.getElementById("spanPrice");
let button = document.getElementById("button-remove");
let incorectValue = document.createElement("span");
button.addEventListener("click", () => {
    span.classList.add("hidden");
    button.classList.add("hidden");
    input.value = "";
});

input.addEventListener("focus", () => {
    input.style.border = "3px solid green";
    input.style.color = "grey";
    incorectValue.remove()
});



input.addEventListener("blur", () => {
    input.style.border = "";
    input.style.color = "";
    input.style.backgroundColor = "";
    if (input.value < 0 || input.value == '') {

        incorectValue.innerText = "Please enter correct price";
        input.after(incorectValue);

    } else {
        span.classList.remove("hidden");
        button.classList.remove("hidden");
        span.innerText = `Текущая цена: ${input.value}`;
    }
});
